﻿using log4net;
using System;
using System.Collections.Concurrent;
using System.Threading;
using wServer.networking;

namespace wServer.realm
{
    using Work = Tuple<Client, PacketID, byte[]>;

    public class NetworkTicker //Sync network processing
    {
        private ILog log = LogManager.GetLogger(nameof(NetworkTicker));

        public RealmManager Manager { get; private set; }

        public NetworkTicker(RealmManager manager)
        {
            Manager = manager;
        }

        public void AddPendingPacket(Client client, PacketID id, byte[] packet)
        {
            pendings.Enqueue(new Work(client, id, packet));
        }

        private static ConcurrentQueue<Work> pendings = new ConcurrentQueue<Work>();
        private static SpinWait loopLock = new SpinWait();

        public void TickLoop()
        {
            log.Info("Network loop started.");
            Work work;
            while (true)
            {
                try
                {
                    if (Manager.Terminating) break;
                    loopLock.Reset();
                    while (pendings.TryDequeue(out work))
                    {
                        try
                        {
                            if (Manager.Terminating) return;
                            if (work.Item1.Stage == ProtocalStage.Disconnected)
                            {
                                Client client;
                                var accId = work.Item1?.Account?.AccountId;
                                if (accId != null)
                                    Manager.Clients.TryRemove(work.Item1.Id, out client);
                                continue;
                            }
                            try
                            {
                                Packet packet = Packet.Packets[work.Item2].CreateInstance();
                                packet.Read(work.Item1, work.Item3, 0, work.Item3.Length);
                                work.Item1.ProcessPacket(packet);
                            }
                            catch (Exception ex)
                            {
                                log.Error(ex);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex);
                        }
                    }
                    while (pendings.Count == 0 && !Manager.Terminating)
                    loopLock.SpinOnce();
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }
            log.Info("Network loop stopped.");
        }
    }
}
